// PLAYER

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";

var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
window.onYouTubeIframeAPIReady = function onYouTubeIframeAPIReady() {
player = new YT.Player('player', {
  height: '360',
  width: '640',
  videoId: 'WZQEty32FcE',
  events: {
    'onReady': onPlayerReady,
    'onStateChange': onPlayerStateChange
  }
});
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  var body = document.body;
  if (event.data == 1) {
    body.classList.add("reading");
  } else {
    body.classList.remove("reading");
  }
}
function stopVideo() {
player.stopVideo();
}



document.querySelectorAll('.bouton-chapitre').forEach(item => {


    item.addEventListener('click', event => {

        event.preventDefault();
        let time = parseFloat(item.dataset.timestamp);

        player.seekTo(time).playVideo();
        document.querySelectorAll('.bouton-chapitre').forEach(itembis => {
                itembis.classList.remove("current");
            });
            item.classList.add('current');

    });

})


function checkTime() {
    let currentTime = player.getCurrentTime();
    document.querySelectorAll('.bouton-chapitre').forEach(item => {
        let itemTime = parseFloat(item.dataset.timestamp);
        if (currentTime >= itemTime) {
            document.querySelectorAll('.bouton-chapitre').forEach(itembis => {
                itembis.classList.remove("current");
            });
            item.classList.add('current');
        }
    });
}


var listening = setInterval(checkTime, 1000);


